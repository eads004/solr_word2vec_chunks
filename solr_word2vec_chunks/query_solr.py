#!/home/spenteco/anaconda2/envs/py3/bin/python
## -*- coding: utf-8 -*-

import sys, requests, json, re
from lxml import etree
from io import StringIO

SOLR_URL = 'http://localhost:8984'

PATH_TO_TOKEN_DATA = '/home/spenteco/1/acls_solr_chunks/token_data/'
WORD2VEC_URL = ''

def get_expanded_query_terms(query_terms):
    
    results = []
    
    for q in query_terms.split(' '):
        
        url = 'https://ada.artsci.wustl.edu/pos_word_distances/index.php?lemma=' + q
    
        r = requests.get(url)
        
        parser = etree.HTMLParser()
        tree = etree.parse(StringIO(r.text), parser)
        
        line_found = False
        for pre in tree.xpath('//pre'):
            for line in pre.text.split('\n'):
                if '---------------------------' in line:
                    line_found = True
                elif line_found == True:
                    if line.strip() > '':
                        cols = [c for c in re.split('\s+', line.strip()) if c > '']
                        results.append(cols[0])
            
    return ' '.join(results)

def make_solr_post(words_to_search, which_to_use):
    
    solr_terms = []
    
    for w in words_to_search.split(' '):
        solr_terms.append(which_to_use + ':"' + w + '"')
        
    solr_url_etc = SOLR_URL + '/solr/eebotcp_chunks/select'
    post_data = {'q': ' '.join(solr_terms), 'rows': 100}
                    
    return solr_url_etc, post_data

def query_solr_post(solr_url_etc, post_data):
    
    r = requests.get(solr_url_etc, data=post_data)
    
    return json.loads(r.text)
    
def get_snippet(tcp_id, a, b, words_to_search, which_to_use):
    
    tokens = open(PATH_TO_TOKEN_DATA + tcp_id + '.tsv', 
                'r', encoding='utf-8').read().split('\n')[a: b + 1]
                
    snippet_tokens = []
    
    for t in tokens:
        
        cols = t.split('\t')
        if len(cols) == 3:
            
            is_matched = False
            if which_to_use == 'orig':
                if cols[0].lower() in words_to_search:
                    is_matched = True
            if which_to_use == 'reg':
                if cols[1].lower() in words_to_search:
                    is_matched = True
            if which_to_use == 'lemma':
                if cols[2].lower() in words_to_search:
                    is_matched = True
                    
            if is_matched == True:
                snippet_tokens.append('<span class="hilite">' + cols[0] + '</span>')
            else:
                snippet_tokens.append(cols[0])
            
    return ' '.join(snippet_tokens)
    
def get_detailed_results(docs, words_to_search, which_to_use):
    
    detailed_results = []
    
    for d in docs:
        
        detailed_results.append(
            '<div class="result">\n' + \
                '<div class="result_citation">' + \
                    '<span class="result_author">' + d['author'][:40] + '</span>. ' + \
                    '<span class="result_title">' + d['title'][:60] + '</span>. ' + \
                    '<span class="result_date">' + d['date'] + '</span>. ' + \
                    '<span class="result_tcp_id">' + d['tcp_id'] + '</span>. \n' + \
                    '<span class="result_a">[' + str(d['starting_token_n']) + '</span>:\n' + \
                    '<span class="result_b">' + str(d['ending_token_n']) + ']</span>\n' + \
                '</div>\n' + \
                '<div class="snippet">' + \
                    get_snippet(d['tcp_id'], 
                                d['starting_token_n'], 
                                d['ending_token_n'],
                                words_to_search,
                                which_to_use) + \
                '</div>\n' + \
            '</div>\n')
            
    return ''.join(detailed_results)
    
if __name__ == "__main__":
    
    query_terms = sys.argv[1]
    expand_query_terms = sys.argv[2]
        
    #print('BEFORE query_terms', query_terms,  
    #        'expand_query_terms', expand_query_terms) 
        
    if expand_query_terms == 'True':
        query_terms = get_expanded_query_terms(query_terms)
    
    words_to_search = []
    for q in query_terms.split(' '):
        words_to_search.append(q.split('_')[0])
        
    words_to_search = ' '.join(words_to_search)
        
    #print('AFTER query_terms', query_terms, 
    #        'words_to_search', words_to_search, 
    #        'expand_query_terms', expand_query_terms) 

    #solr_query = make_solr_query(words_to_search, 'lemma')

    #solr_response = query_solr(solr_query)
    
    solr_url_etc, post_data = make_solr_post(words_to_search, 'lemma')

    solr_response = query_solr_post(solr_url_etc, post_data)
    
    detailed_results = get_detailed_results(solr_response['response']['docs'],
                                                set(words_to_search.split(' ')),
                                                'lemma')
                                                
    detailed_results = '<div id="expanded_query_terms">' + \
                            query_terms + \
                        '</div>' + \
                        detailed_results 
    
    print(detailed_results)
