#!/home/spenteco/anaconda2/envs/py3/bin/python

import pysolr, glob, time
from lxml import etree

#    --------------------------------------------------------------------
#
#    --------------------------------------------------------------------

def loadCatalog(pathToCatalog):

    catalog = {}
    temp = open(pathToCatalog, 'r', encoding='utf-8').read().split('\n')
    for t in temp:
        tParts = t.split('\t')
        if len(tParts) > 2:
            
            eeboKey = tParts[0]
            date = tParts[1]
            author = tParts[2]
            title = tParts[3]
            
            catalog[eeboKey] = {'date': date, 'author': author, 'title': title}
            
    print(len(catalog))
    print(catalog['A00001'])
            
    return catalog

#    --------------------------------------------------------------------
#
#    --------------------------------------------------------------------


def loadIndex(baseUrl, catalog, collection, pathToFiles):

    paths_to_files = glob.glob(pathToFiles)

    print(len(paths_to_files))

    solr = pysolr.Solr(baseUrl + collection + '/', timeout=600)
    
    for pn, p in enumerate(paths_to_files):
        
        tcp_id = p.split('/')[-1].split('.')[0]
        
        try:
            
            start_time = time.time()
            
            author = catalog[tcp_id]['author']
            title = catalog[tcp_id]['title']
            date = catalog[tcp_id]['date']
            
            orig = []
            reg = []
            lemma = []
            
            tree = etree.parse(p)
            
            for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                
                if w.text != None:
                    
                    orig.append(w.text)
                    
                    if w.get('reg') != None:
                        reg.append(w.get('reg'))
                    else:
                        reg.append(w.text)
                        
                    if w.get('lemma') != None:
                        lemma.append(w.get('lemma'))
                    else:
                        lemma.append(w.text)
                        
            for a in range(0, len(orig)):
                
                classes = [orig[a].lower(), reg[a].lower(), lemma[a].lower()]
                classes = list(set(classes))
            
            a = 0
            b = 500
            
            while b < len(orig):
                
                orig_content = ' '.join(orig[a: b])
                reg_content = ' '.join(reg[a: b])
                lemma_content = ' '.join(reg[a: b])
                
                doc = {'tcp_id': tcp_id,
                        'starting_token_n': a,
                        'ending_token_n': b,
                        'orig': orig_content,
                        'reg': reg_content,
                        'lemma': lemma_content,
                        'author': author,
                        'date': date,
                        'title': title}
                        
                solr.add([doc])
                
                a = a + 100
                b = b + 100

            f = open('token_data/' + tcp_id + '.tsv', 'w', encoding='utf-8')
                
            for a in range(0, len(orig)):
                f.write(orig[a] + '\t' + reg[a] + '\t' + lemma[a] + '\n')
                
            f.close()
            
            stop_time = time.time()
        
            print(pn, p, (stop_time - start_time))
                    
            
        except KeyError:
            print('bypassing', tcp_id)
            
    f.close()

    print('OPTIMIZING')

    solr.optimize()
    
#    --------------------------------------------------------------------
#
#    --------------------------------------------------------------------

catalog = loadCatalog('EEBO_metadata.tsv')
baseUrl = 'http://localhost:8984/solr/'
index_name = 'eebotcp_chunks'

loadIndex(baseUrl, 
            catalog,
            index_name, 
            '/home/data/eebotcp/texts/**/*.xml')


