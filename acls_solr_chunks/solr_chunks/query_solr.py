#!/home/spenteco/anaconda2/envs/py3/bin/python
## -*- coding: utf-8 -*-

import sys, requests, json

#PORT_NUMBER = '8983'
PORT_NUMBER = '8984'
PATH_TO_TOKEN_DATA = '/home/spenteco/1/acls_solr_chunks/token_data/'

def make_solr_query(words_to_search, which_to_use):
    
    solr_terms = []
    
    for w in words_to_search.split(' '):
        solr_terms.append(which_to_use + ':"' + w + '"')
        
    solr_query = 'http://localhost:' + PORT_NUMBER + '/solr/eebotcp_chunks/select?q=' + \
                    ' '.join(solr_terms) + '&rows=100'
                    
    return solr_query
    
def query_solr(solr_query):
    
    r = requests.get(solr_query)
    
    return json.loads(r.text)
    
def get_snippet(tcp_id, a, b, words_to_search, which_to_use):
    
    tokens = open(PATH_TO_TOKEN_DATA + tcp_id + '.tsv', 
                'r', encoding='utf-8').read().split('\n')[a: b + 1]
                
    snippet_tokens = []
    
    for t in tokens:
        
        cols = t.split('\t')
        if len(cols) == 3:
            
            is_matched = False
            if which_to_use == 'orig':
                if cols[0].lower() in words_to_search:
                    is_matched = True
            if which_to_use == 'reg':
                if cols[1].lower() in words_to_search:
                    is_matched = True
            if which_to_use == 'lemma':
                if cols[2].lower() in words_to_search:
                    is_matched = True
                    
            if is_matched == True:
                snippet_tokens.append('<span class="hilite">' + cols[0] + '</span>')
            else:
                snippet_tokens.append(cols[0])
            
    return ' '.join(snippet_tokens)
    
def get_detailed_results(docs, words_to_search, which_to_use):
    
    detailed_results = []
    
    for d in docs:
        
        detailed_results.append(
            '<div class="result">\n' + \
                '<div class="result_citation">' + \
                    '<span class="result_author">' + d['author'][:40] + '</span>. ' + \
                    '<span class="result_title">' + d['title'][:60] + '</span>. ' + \
                    '<span class="result_date">' + d['date'] + '</span>. ' + \
                    '<span class="result_tcp_id">' + d['tcp_id'] + '</span>. \n' + \
                    '<span class="result_a">[' + str(d['starting_token_n']) + '</span>:\n' + \
                    '<span class="result_b">' + str(d['ending_token_n']) + ']</span>\n' + \
                '</div>\n' + \
                '<div class="snippet">' + \
                    get_snippet(d['tcp_id'], 
                                d['starting_token_n'], 
                                d['ending_token_n'],
                                words_to_search,
                                which_to_use) + \
                '</div>\n' + \
            '</div>\n')
            
    return ''.join(detailed_results)
    
if __name__ == "__main__":
    
    words_to_search = sys.argv[1]
    which_to_use = sys.argv[2]
    
    #print('words_to_search', words_to_search, 'which_to_use', which_to_use) 

    solr_query = make_solr_query(words_to_search, which_to_use)
    
    #print('solr_query', solr_query)
    
    solr_response = query_solr(solr_query)
    
    #print('solr_response', solr_response)
    
    #print(solr_response['response']['numFound'], solr_response['response']['numFoundExact'])
    
    detailed_results = get_detailed_results(solr_response['response']['docs'],
                                                set(words_to_search.split(' ')),
                                                which_to_use)
    
    print(detailed_results)
