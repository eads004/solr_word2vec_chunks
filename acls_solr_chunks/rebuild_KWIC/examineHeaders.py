#!/usr/bin/python
## coding: utf-8

import os, sys, re, codecs
from lxml import etree
from unidecode import unidecode

numberOfFiles = 0

# ----------------------------------------------------------------------
#   tar -zxvf 
# ----------------------------------------------------------------------

def examineHeaders(folderName):

    global numberOfFiles

    for inputFileName in os.listdir(folderName):

        if os.path.isdir(folderName + inputFileName) == True:
            examineHeaders(folderName + inputFileName + '/')
        else:
        
            if inputFileName.endswith('.headed.xml') == True:

                numberOfFiles += 1

                tree = etree.parse(folderName + inputFileName)

                headers = tree.xpath('//HEADER')

                if len(headers) != 1:
                    print 'ERROR', (folderName + inputFileName), 'len(headers)', len(headers)
                else:
                    
                    biblFulls = headers[0].xpath('descendant::BIBLFULL')
                    
                    if len(biblFulls) != 1:
                        print 'ERROR', (folderName + inputFileName), 'len(biblFulls)', len(biblFulls)
                    else:

                        titles = biblFulls[0].xpath('descendant::TITLE')
                        authors = biblFulls[0].xpath('descendant::AUTHOR')
                        pubplaces = biblFulls[0].xpath('descendant::PUBPLACE')
                        publishers = biblFulls[0].xpath('descendant::PUBLISHER')
                        dates = biblFulls[0].xpath('descendant::DATE')
                    
                        outputString = folderName + inputFileName + '\t' + inputFileName.replace('.headed.xml', '')
                        
                        s = ''
                        for t in titles:
                            if s > '':
                                s = s + '|'
                            s = s + t.text
                        if s == '':
                            s = ' '

                        outputString = outputString + '\t' + s
                        
                        s = ''
                        for t in authors:
                            if s > '':
                                s = s + '|'
                            s = s + t.text
                        if s == '':
                            s = ' '

                        outputString = outputString + '\t' + s
                        
                        s = ''
                        for t in pubplaces:
                            if s > '':
                                s = s + '|'
                            s = s + t.text
                        if s == '':
                            s = ' '

                        outputString = outputString + '\t' + s
                        
                        s = ''
                        for t in publishers:
                            if s > '':
                                s = s + '|'
                            s = s + t.text
                        if s == '':
                            s = ' '

                        outputString = outputString + '\t' + s
                        
                        s = ''
                        for t in dates:
                            if s > '':
                                s = s + '|'
                            s = s + t.text
                        if s == '':
                            s = ' '

                        outputString = outputString + '\t' + s

                        print outputString

# ----------------------------------------------------------------------
#   MAIN
#
# ----------------------------------------------------------------------

examineHeaders('/home/spenteco/0/eebo_022015/')

print
print 'numberOfFiles', numberOfFiles
print
