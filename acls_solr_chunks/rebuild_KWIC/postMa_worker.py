#!/usr/bin/env python

import os, sys, re, codecs, csv, commands, sqlite3
import logging
import pika
import time

logging.basicConfig(format = '%(asctime)-25s %(message)s', level=20)

#   --------------------------------------------------------------------
#   plaintext           should we use what was input to morphadorner, and not this?
#   plaintext_lem
#   plaintext_reg
#   plaintext_reg_pos
#   --------------------------------------------------------------------

def callback(ch, method, properties, body):
    
    ch.basic_ack(delivery_tag = method.delivery_tag)
    
    logging.info(' [x] Received ' + body)

    pathToInputFile = body.split('|')[0]
    outputFolder = body.split('|')[1]

    startingTime = time.time()

    fileName = pathToInputFile.split('/')[-1]

    inData = codecs.open(pathToInputFile, 'r', encoding='utf-8').read().split('\n')

    plaintext = []
    plaintext_lem = []
    plaintext_reg = []
    plaintext_reg_pos = [] 

    for i in inData:

        if i.strip() > '':

            cols = i.split('\t')
        
            tok = cols[0]
            spe = cols[1]
            pos = cols[2]
            reg = cols[3]
            lem = cols[4]
        
            plaintext.append(spe)
            plaintext_lem.append(lem)
            plaintext_reg.append(reg)
            plaintext_reg_pos.append(reg + '_' + pos)

    outF = codecs.open(outputFolder + 'plaintext/' + fileName, 'w', encoding='utf-8')
    outF.write(' '.join(plaintext))
    outF.close()

    outF = codecs.open(outputFolder + 'plaintext_lem/' + fileName, 'w', encoding='utf-8')
    outF.write(' '.join(plaintext_lem))
    outF.close()

    outF = codecs.open(outputFolder + 'plaintext_reg/' + fileName, 'w', encoding='utf-8')
    outF.write(' '.join(plaintext_reg))
    outF.close()

    outF = codecs.open(outputFolder + 'plaintext_reg_pos/' + fileName, 'w', encoding='utf-8')
    outF.write(' '.join(plaintext_reg_pos))
    outF.close()

    endingTime = time.time()

    logging.info(' processed ' + pathToInputFile + ' in ' + str(endingTime - startingTime) + ' seconds')

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------


connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
        
channel = connection.channel()

channel.queue_declare(queue='morphadorner_tcp', durable=True)

logging.info(' [*] Waiting for messages. To exit press CTRL+C')

channel.basic_qos(prefetch_count=1)

channel.basic_consume(callback,
                      queue='morphadorner_tcp')

channel.start_consuming()
