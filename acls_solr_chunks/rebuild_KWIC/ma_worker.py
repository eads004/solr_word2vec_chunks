#!/usr/bin/env python

import os, sys, re, codecs, csv, commands, sqlite3
import logging
import pika
import time

logging.basicConfig(format = '%(asctime)-25s %(message)s', level=20)

#   --------------------------------------------------------------------
#   
#   --------------------------------------------------------------------

def executeCommand(cmd, printCommand):

    if printCommand == True:
        print cmd

    results = commands.getoutput(cmd)

    if printCommand == True:
        print results

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

def callback(ch, method, properties, body):
    
    ch.basic_ack(delivery_tag = method.delivery_tag)
    
    logging.info(' [x] Received ' + body)

    pathToInputFile = body.split('|')[0]
    outputFolder = body.split('|')[1]

    startingTime = time.time()

    #cmd = 'cd /home/spenteco/1/basu/rebuild_eebo_tcp/morphadorner-2.0.1; ./adornplainemetext ' + outputFolder + ' ' + pathToInputFile
    cmd = 'cd /home/spenteco/rebuild_eebo_tcp/morphadorner-2.0.1; ./adornplainemetext ' + outputFolder + ' ' + pathToInputFile

    executeCommand(cmd,True)

    endingTime = time.time()

    logging.info(' processed ' + pathToInputFile + ' in ' + str(endingTime - startingTime) + ' seconds')

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
        
channel = connection.channel()

channel.queue_declare(queue='morphadorner_tcp', durable=True)

logging.info(' [*] Waiting for messages. To exit press CTRL+C')

channel.basic_qos(prefetch_count=1)

channel.basic_consume(callback,
                      queue='morphadorner_tcp')

channel.start_consuming()



