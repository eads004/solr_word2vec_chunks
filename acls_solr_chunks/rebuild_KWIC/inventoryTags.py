#!/usr/bin/python
## coding: utf-8

import os, sys, re, codecs
from lxml import etree
from unidecode import unidecode

tags = {}
numberOfFiles = 0

# ----------------------------------------------------------------------
#   tar -zxvf 
# ----------------------------------------------------------------------

def inventoryTags(folderName):
    
    global tags
    global numberOfFiles

    for inputFileName in os.listdir(folderName):

        if os.path.isdir(folderName + inputFileName) == True:
            inventoryTags(folderName + inputFileName + '/')
        else:
        
            #if inputFileName.endswith('.headed.xml') == True:
            if inputFileName.find('A12782') > -1:

                numberOfFiles += 1

                tree = etree.parse(folderName + inputFileName)

                bodyNodes = tree.xpath('/ETS/EEBO')

                if len(bodyNodes) != 1:
                    print 'ERROR', 'inputFileName', inputFileName, 'len(bodyNodes)', len(bodyNodes)
                else:
                    allNodes = bodyNodes[0].xpath('descendant::node()')
                    for a in allNodes:
                        if isinstance(a, etree._Element) == True:
            
                            #if a.tag == 'DATE':
                            #    print 'inputFileName', inputFileName, etree.tostring(a)

                            try:
                                tags[a.tag] += 1
                            except KeyError:
                                tags[a.tag] = 1

# ----------------------------------------------------------------------
#   MAIN
#
# ----------------------------------------------------------------------

inventoryTags(sys.argv[1])

print
for k in sorted(tags.keys()):
    print k, tags[k]
print
print 'numberOfFiles', numberOfFiles
print
