#!/usr/bin/env python

import logging
import pika
import sys, os, sqlite3

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

#inputFolder = '/home/spenteco/1/basu/new_eebo_plaintext/'
inputFolder = '/home/data/new_raw_eebo_plaintext/'

#outputFolder = '/home/spenteco/1/basu/new_eebo_morphadorned/'
outputFolder = '/home/data/new_eebo_morphadorned/'

logging.basicConfig(format = '%(asctime)-25s %(message)s', level=logging.INFO)

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
        
channel = connection.channel()

channel.queue_declare(queue='morphadorner_tcp', durable=True)

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

for inputFileName in os.listdir(inputFolder):
    
    messageContent = inputFolder + inputFileName + '|' + outputFolder

    logging.warning('queueing task for ' + messageContent)

    channel.basic_publish(exchange='',
                          routing_key='morphadorner_tcp',
                          body=messageContent,
                          properties=pika.BasicProperties(
                             delivery_mode = 2,
                          ))

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

connection.close()
        

