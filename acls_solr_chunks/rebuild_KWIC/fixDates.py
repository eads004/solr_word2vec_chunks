#!/usr/bin/python
## coding: utf-8

import os, sys, re, codecs

dateFixes = [   ['MDCXCVIII.', '1698'],
                ['16-?]', ''],
                ['l668.', '1668'],
                ['16--?]', ''],
                [u'169⁵/₆.', '1695'],
                ['M DC XCI.', '1691'],
                ['M.DC.XXVIII.', '1628'],
                ['l689.', '1689'],
                ['M DC XLII.', '1642'],
                ['In the yere of our lord god M. CCCCC. and .xxxix.', '1539'],
                ['unknown', ''],
                [u'169⁵/₆.', '1595'],
                ['16--?]', ''],
                ['166[blank].', '1660'],
                ['Anno.', ''],
                ['Anno.', ''],
                ['Anno.', ''],
                ['15--?]', ''],
                ['Anno.', ''],
                ['l642.', '1642'],
                ['[l642]', '1642'],
                ['153-?].', '1530'],
                ['16--?]', ''],
                ['[16--?]', ''],
                ['[16--?]', ''],
                ['[16--?]', ''],
                ['16--?]', ''],
                ['[15--?]', ''],
                ['166-].', '1660'],
                ['Imprinted Anno Dom. cIc. Ic. c.xxiiii.', ''],
                ['167-]', '1670'],
                ['15--?]', ''],
                [' Anno Dom. 15[70?]', '1570'],
                ['Printed in the year, [1]653.', '1653'],
                ['Printed Anno M. DC. XL.', '1640'],
                ['printed in the year, 16[62]', '1662'],
                ['Anno. 1.5.5.9.', '1559'],
                ['[f1554]', '1554'],
                ['A. M. D. LII.', '1552'],
                ['16--?]', ''],
                ['Sent from the army Novemb. 18. to be printed and published.', ''],
                ['16--]', ''],
                ['Anno .M.D.XL..', '1540'],
                ['unknown', ''],
                ['May 21. 164[7]', '1647'],
                ['MDCLXX.', '1670'],
                ['MDCXCI.', '1691'],
                ['printed in the year MDCLXXXIX. and are to be sold at several booksellers. Price two pence.', '1689'],
                ['In the yere of our lorde god. M.D.xxvi. the xxvii. day of Iuly]', '1526'],
                ['l654.', '1654'],
                ['Anno M. D. L. ix.', '1559'],
                ['printed in the year, 168[blank]', '1680'],
                ['167[4- 9?]', '1674'],
                ['4⁰.', ''],
                ['l642.', '1642'],
                ['MDCLXXXVII.', '1687'],
                ['[16--?]', ''],
                ['169-]', '1690'],
                ['168-]', '1680'],
                ['l687.', '1687'],
                ['l688.', '1688'],
                ['in the yeare of our Lorde God. 1.5.4.6]', '1546'],
                ['M.DC.LXXII.', '1672'],
                ['Anno 1.5.62.', '1562'],
                ['168-]', '1680'],
                ['[168?]', '1680'],
                ['Anno Domini. 1.5.5.4, 18. Decembris.', '1554'],
                ['printed in the year, 169[4?]', '1694'],
                ['Printed in the year one thousand seven hundred, &c.', ''],
                ['[165-]', '1650'],
                ['[16--?]', ''],
                ['16--]', ''],
                ['16--]', ''],
                ['[167-]', '1670'],
                ['Me[n]se Iulij, M.D.XLIX.', '1549'],
                ['[16-?]', ''],
                ['l674.', '1674'],
                ['l688.', '1688'],
                ['Anno 1.5.5.4. 26 Decembris.', '1554'],
                ['Printed in the yeare, 16[49]', '1649'],
                ['Anno.', ''],
                ['160]', ''],
                ['[l683]', '1683'],
                ['16--?]', ''],
                ['unknown', ''],
                ['Anno.', ''],
                ['16--?]', ''],
                ['169-]', '1690'],
                ['[l642]', '1642'],
                ['168-]', '1680'],
                ['MDCLXXXIV.', '1684'],
                ['l691.', '1691'],
                ['Anno Domini. 1.5.5.4. 24. Iulij]', '1554'],
                ['l647.', '1647'],
                ['[16--]', ''],
                [u'169⁵/₆.', '1695'],
                ['printed in the year, 16[47]', '1647'],
                ['[c1550.]]', '1550'],
                ['Anno. M.D.LIIII. mense Septembri]', '1554'],
                ['MDCC.', ''],
                ['Printed in the year 16[95]', '1695'],
                ['A.D. 1[651?]', '1651'],
                ['16--?]', ''],
                ['A1692', '1692'],
                ['unknown', ''],
                ['Anno.', ''],
                ['16--?]', ''],
                ['Anno.', '']]

def cleanDate(s):

    global dateFixes

    resultDate = ''
    resultOkay = False

    s1 = s.replace('.', '').replace('?', '')

    try:
        resultDate = str(int(s1))
        resultOkay = True

        return resultDate, resultOkay
    except:
        pass

    s2 = re.split('(\[|\])', s1)
    for p in s2:
        if len(p) == 4:
            try:
                resultDate = str(int(p))
                resultOkay = True

                return resultDate, resultOkay
            except:
                pass

    s2 = re.split('(\W)', s)
    for p in s2:
        if len(p) == 4:
            try:
                resultDate = str(int(p))
                resultOkay = True

                return resultDate, resultOkay
            except:
                pass

    if s.endswith('-?]') == True:
        for p in s2:
            if len(p) == 3:
                try:
                    resultDate = str(int(p)) + '0'
                    resultOkay = True

                    return resultDate, resultOkay
                except:
                    pass

    s2 = s1.replace('[', '').replace(']', '')
    if len(s2) == 4:
        try:
            resultDate = str(int(s2))
            resultOkay = True

            return resultDate, resultOkay
        except:
            pass

    for d in dateFixes:
        if d[0] == s:
            resultDate = d[1]
            resultOkay = True
            break

    return resultDate, resultOkay

#   --------------------------------------------------------------------

def main():

    inData = codecs.open('OUT_examineHeaders.csv', 'r', encoding='utf-8').read().split('\n')

    for i in inData:

        if i.strip() > '' and i.startswith('numberOfFiles') == False:

            cols = i.split('\t')    
            dateColumn = cols[-1]

            date, dateOkay = cleanDate(dateColumn)
        
            if dateOkay == True:
                print 'OK', dateColumn, date
                #pass
            else:
                print 'ERROR', dateColumn
                #if dateColumn.strip() > '':
                #    print dateColumn
                    
if __name__ == "__main__":
    main()
