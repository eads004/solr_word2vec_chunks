#!/usr/bin/env python

import logging
import pika
import time

logging.basicConfig(format = '%(asctime)-25s %(message)s', level=20)

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
        
channel = connection.channel()

channel.queue_declare(queue='morphadorner_tcp', durable=True)

logging.info(' [*] Waiting for messages. To exit press CTRL+C')

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

def callback(ch, method, properties, body):
    
    logging.info(' [x] Flushing ' + body)
    
    ch.basic_ack(delivery_tag = method.delivery_tag)

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------
    

channel.basic_qos(prefetch_count=1)

channel.basic_consume(callback,
                      queue='morphadorner_tcp')

channel.start_consuming()



