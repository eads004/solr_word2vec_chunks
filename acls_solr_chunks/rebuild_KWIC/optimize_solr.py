#!/usr/bin/python

import sys, os, codecs, re
import pysolr
#    --------------------------------------------------------------------
#
#    --------------------------------------------------------------------


def optimize_index(baseUrl, collection, pathToFiles):

    print 'starting', collection

    solr = pysolr.Solr(baseUrl + collection + '/', timeout=600)
            
    solr.optimize()

    print 'done', collection
    
#    --------------------------------------------------------------------
#
#    --------------------------------------------------------------------

baseUrl = 'http://localhost:8983/solr/'
optimize_index(baseUrl, 'plaintext', '/home/spenteco/0/eebo_012617/post_morphadorned/plaintext/') 
optimize_index(baseUrl, 'plaintext_lem', '/home/spenteco/0/eebo_012617/post_morphadorned/plaintext_lem/')
optimize_index(baseUrl, 'plaintext_reg', '/home/spenteco/0/eebo_012617/post_morphadorned/plaintext_reg/')
optimize_index(baseUrl, 'plaintext_reg_pos', '/home/spenteco/0/eebo_012617/post_morphadorned/plaintext_reg_pos/')
