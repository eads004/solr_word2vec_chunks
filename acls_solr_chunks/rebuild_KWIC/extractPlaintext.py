#!/usr/bin/python
## coding: utf-8

import os, sys, re, codecs
from lxml import etree
from unidecode import unidecode

number_of_texts = 0

# ----------------------------------------------------------------------
#   
# ----------------------------------------------------------------------

def extractPlaintext(inputFolder, outputFolder):

    global number_of_texts

    for inputFileName in os.listdir(inputFolder):

        if os.path.isdir(inputFolder + inputFileName) == True:
            extractPlaintext(inputFolder + inputFileName + '/', outputFolder)
        else:
            if inputFileName.endswith('.headed.xml') == True:

                number_of_texts += 1

                tree = etree.parse(inputFolder + inputFileName)

                texts = tree.xpath('/ETS/EEBO')

                print 'inputFileName', inputFileName

                outF = codecs.open(outputFolder + inputFileName.replace('.headed.xml', '.txt'), 'w', encoding='utf-8')

                for t in texts:

                    textNodes = t.xpath('descendant::text()')
                    
                    
                    for n in textNodes:
                        if n.getparent().tag not in ['BIBNO', 'FIGDESC', 'FIGURE', 'IDG', 'STC', 'VID']:
                            
                            outF.write(re.sub('\s+', ' ', n.replace(u'∣', '')))
                
                outF.close()

# ----------------------------------------------------------------------
#   ./extractPlaintext.py inputFolder outputFolder
#       (folder names ending with '/')
#
#   ./extractPlaintext.py /home/spenteco/0/eebo_022015/ /home/spenteco/0/new_raw_eebo_plaintext/
# ----------------------------------------------------------------------

extractPlaintext(sys.argv[1], sys.argv[2])

print
print 'number_of_texts', number_of_texts
