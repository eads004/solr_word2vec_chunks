#!/usr/bin/python

import sys, os, codecs, re
import simplejson as json
import pysolr

#    --------------------------------------------------------------------
#
#    --------------------------------------------------------------------

def loadCatalog(pathToCatalog):

    catalog = {}
    temp = codecs.open(pathToCatalog, 'r', encoding='utf-8').read().split('\n')
    for t in temp:
        tParts = t.split('\t')
        if len(tParts) > 2:
            
            eeboKey = tParts[0]
            date = tParts[1]
            author = tParts[2]
            title = tParts[3]
            
            catalog[eeboKey] = {'date': date, 'author': author, 'title': title}
            
    return catalog

#    --------------------------------------------------------------------
#
#    --------------------------------------------------------------------


def loadIndex(baseUrl, catalog, collection, pathToFiles):

    solr = pysolr.Solr(baseUrl + collection + '/', timeout=600)

    n = 0

    for fileName in os.listdir(pathToFiles):

        n += 1
        
        eeboKey = unicode(fileName.replace('.txt', ''))
        
        author = ''
        date = ''
        title = ''

        try:
            author = catalog[eeboKey]['author']
            date = catalog[eeboKey]['date']
            title = catalog[eeboKey]['title']
        except KeyError:
            author = 'MISSING METADATA'
            date = 'MISSING METADATA'
            title = 'MISSING METADATA'

        content = ''

        try:
            content = codecs.open(pathToFiles + fileName, 'r', encoding='utf-8').read()
        except UnicodeDecodeError:
            print 'addUnicodeDecodeError', collection, fileName, n 

        if content > '':

            content = re.sub('\s+', ' ', content)
            
            if author.strip() == '':
                author = 'NOT SPECIFIED'
            
            if date.strip() == '':
                date = 'NOT SPECIFIED'
            
            if title.strip() == '':
                title = 'NOT SPECIFIED'
                
            doc = {'id': eeboKey, 'author': author, 'date': date, 'title': title,'text': content}
            
            print 'adding', collection, fileName, n
            #print 'author', author, 'date', 'title', title
            
            solr.add([doc])

            
    solr.optimize()
    
#    --------------------------------------------------------------------
#
#    --------------------------------------------------------------------

catalog = loadCatalog('/home/spenteco/EEBO_metadata.tsv')
baseUrl = 'http://talusjr.artsci.wustl.edu:8983/solr/'
loadIndex(baseUrl, catalog, 'plaintext', '/home/data2/eebo_012617/post_morphadorned/plaintext/') 
loadIndex(baseUrl, catalog, 'plaintext_lem', '/home/data2/eebo_012617/post_morphadorned/plaintext_lem/')
loadIndex(baseUrl, catalog, 'plaintext_reg', '/home/data2/eebo_012617/post_morphadorned/plaintext_reg/')
loadIndex(baseUrl, catalog, 'plaintext_reg_pos', '/home/data2/eebo_012617/post_morphadorned/plaintext_reg_pos/')
