#!/usr/bin/python
## coding: utf-8

import os, sys, re, codecs
from lxml import etree
from unidecode import unidecode

numberOfFiles = 0

def loadCatalog():

    catalog = {}
    temp = codecs.open('/home/spenteco/0/lucene/testData/EEBO_metadata.tsv', 'r', encoding='utf-8').read().split('\n')
    for t in temp:
        tParts = t.split('\t')
        if len(tParts) > 2:
            
            eeboKey = tParts[0]
            date = tParts[1]
            author = tParts[2]
            title = tParts[3]
            
            catalog[eeboKey] = {'date': date, 'author': author, 'title': title}
            
    return catalog


# ----------------------------------------------------------------------
#   tar -zxvf 
# ----------------------------------------------------------------------

def inventoryTags(folderName, catalog):

    global numberOfFiles

    for inputFileName in os.listdir(folderName):

        if os.path.isdir(folderName + inputFileName) == True:
            inventoryTags(folderName + inputFileName + '/', catalog)
        else:
        
            if inputFileName.endswith('.headed.xml') == True:

                numberOfFiles += 1

                tree = etree.parse(folderName + inputFileName)

                argumentNodes = tree.xpath('//ARGUMENT')
                figureNodes = tree.xpath('//FIGURE')
                lgNodes = tree.xpath('//LG')

                #if len(argumentNodes) > 6 and len(figureNodes) > 6 and len(lgNodes) > 20:
                if len(figureNodes) > 10 and len(lgNodes) > 20:
                #if len(figureNodes) > 10:

                    try:
                    
                        if catalog[inputFileName.replace('.headed.xml', '')]['date'] < '1600':

                            titlePages = tree.xpath('//DIV1[@TYPE="title page"]')

                            print
                            print folderName + inputFileName, len(argumentNodes), len(figureNodes), len(lgNodes)
                            print catalog[inputFileName.replace('.headed.xml', '')]['date'], catalog[inputFileName.replace('.headed.xml', '')]['title'], catalog[inputFileName.replace('.headed.xml', '')]['author']      

                            if len(titlePages) > 0:
                                print
                                print etree.tostring(titlePages[0], pretty_print=True)

                    except KeyError:

                            titlePages = tree.xpath('//DIV1[@TYPE="title page"]')

                            print
                            print folderName + inputFileName, len(argumentNodes), len(figureNodes), len(lgNodes)
                            print 'MISSING METADATA'      

                            if len(titlePages) > 0:
                                print
                                print etree.tostring(titlePages[0], pretty_print=True)
                        


# ----------------------------------------------------------------------
#   MAIN
#
# ----------------------------------------------------------------------

catalog = loadCatalog()
inventoryTags(sys.argv[1], catalog)

print
print 'numberOfFiles', numberOfFiles
print
