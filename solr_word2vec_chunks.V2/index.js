
function do_go() {

    var words_to_search = $('#words_to_search').val();
    var which_to_use = $('input[name="which_to_use"]:checked').val();
    
    $('#all_results').html('<div class="working_message">Working . . . </div>');
    
    $.get('query_solr.php?words_to_search=' + words_to_search + '&which_to_use=' + which_to_use, 
        function(data) {
            $('#all_results').html(data);
        }
    );
}
