<!DOCTYPE html>
<html>
    <head>
        <title>POS Word Distances</title>
        <meta charset="UTF-8">
        <style>
.spacer {
    display: inline-block;
    width: 4em;
}
        </style>
    </head>
    <body>

        <h1>POS Word Distances</h1>
        
        <div>
            <form action="index.php">
                <label for="fname">lemma_POS:</label> <input type="text" id="lemma" name="lemma" value=""><br/><br/>
                <span class="spacer"> </span><input type="submit" value="Submit"/>
            </form> 

            <p>Enter a lemma_POS value (i.e., a lemma and a POS joined by an underscore, and click submit.  You can enter more than one lemma_POS so long as you separate them with spaces.</p>
            <p>The best POS to try are:
                <table>
                    <tr><td>n</td><td>noun</td></tr>
                    <tr><td>v</td><td>verb</td></tr>
                    <tr><td>j</td><td>adjective</td></tr>
                </table>
            </p>
            <p>You might also try one or more of 
            spear_n circle_n supplication_n heretic_n skin_n base_n tribute_n equity_n sinew_n side_n understand_v bury_v murder_v clothe_v blaspheme_v seek_v confirm_v imitate_v behold_v disperse_v meek_j fit_j base_j fond_j loose_j counterfeit_j foreign_j cunning_j common_j external_j </p>
            
        </div>
        
        <div>
            <pre>
<?php

    if (isset($_GET['lemma'])) {
        
        $lemma = $_GET['lemma'];
        $lemma_parts = explode(' ', $lemma);
        
        $f = fopen('/tmp/POS_word_distances.txt' , 'w');
        
        foreach ($lemma_parts as &$value) {
            fwrite($f, $value . "\n");
        }

        fwrite($f, 'EXIT' . "\n");
        fclose($f);
        
        $output = shell_exec('cat /tmp/POS_word_distances.txt | ./distance_pos  /home/spenteco/1/acls_word_embeddings/POS.eebotcp.vector.bin');
        
        $output = str_replace('Enter word or sentence (EXIT to break):', '', $output);
        
        echo $output;
    }
?>
            </pre>
        </div>

    </body>
</html> 
